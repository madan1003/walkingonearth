package com.example.wone

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.wone.home.HomeFragment

/*
* We are opening basic Main activity
* with a Framgent having all the implementations
* */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().add(R.id.frame_layout, HomeFragment()).commit()
    }
}