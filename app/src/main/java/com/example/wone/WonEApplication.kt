package com.example.wone

import android.app.Application

/*
* This class is being user to do inital setup like
* crashlytics initiatialization
* any other 3rd party sdk initializations
* */
class WonEApplication: Application() {
    override fun onCreate() {
        super.onCreate()
    }
}