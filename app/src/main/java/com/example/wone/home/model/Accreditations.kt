package com.example.wone.home.model

import com.example.wone.Adapter.BaseTModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/*
* Data holding class
* */
class Accreditations(

    @SerializedName("name")
    @Expose
    var name: String? = null,

    @SerializedName("photo")
    @Expose
    var photo: String? = null
) : BaseTModel()