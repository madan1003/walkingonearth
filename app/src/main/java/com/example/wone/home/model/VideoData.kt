package com.example.wone.home.model

import com.example.wone.Adapter.BaseTModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VideoData(
    @SerializedName("name")
    @Expose
    var name: String? = null,

    @SerializedName("url")
    @Expose
    var url: String? = null,

    @SerializedName("photo")
    @Expose
    var photo: String? = null
) : BaseTModel()