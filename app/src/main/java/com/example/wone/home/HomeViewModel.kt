package com.example.wone.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.wone.Adapter.BaseTModel
import com.example.wone.R
import com.example.wone.home.model.Accreditations
import com.example.wone.home.model.HeaderDataModel
import com.example.wone.home.model.HomeDataModel
import com.example.wone.home.model.VideoData
import com.example.wone.utility.RestClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/*
* This is view Model
* Here I have not used hild to provide network module or any other dependency
* I have created a static rest client and used the same.
*
* We can change this clean architecture with the help of use case and repository pattern
*
* due to time constraint have not done this.
* */
class HomeViewModel : ViewModel() {
    val liveData = MutableLiveData<List<BaseTModel>>()
    val error = MutableLiveData<String>()

    init {
        requestData()
    }

    private fun requestData() {

        RestClient.getInstance().getHomeData().enqueue(object : Callback<HomeDataModel> {
            override fun onFailure(call: Call<HomeDataModel>, t: Throwable) {
                Log.d("TAG", "Failure")
                error.value = ""
            }

            override fun onResponse(call: Call<HomeDataModel>, response: Response<HomeDataModel>) {

                if (response.isSuccessful && response.body() != null) {
                    Log.d("TAG", "Success")
                    prepareData(response.body()!!)
                } else {
                    error.value = ""
                }
            }
        })


    }

    private fun prepareData(body: HomeDataModel) {
        val list = ArrayList<BaseTModel>()

        list.add(
            HeaderDataModel(
                body.teacherId,
                body.firstName,
                body.lastName,
                body.level,
                body.photo,
                body.introVideo,
                body.bio,
                body.teachingAccreditations
            ).apply { layoutResId = R.layout.home_header_layout }
        )

        val accreditations = body.teachingAccreditations?.split("\n")
        if (accreditations != null) {
            for (item in accreditations) {
                list.add(Accreditations(item).apply {
                    layoutResId = R.layout.accreditations
                })
            }
        }

        if (!body.introVideo.isNullOrEmpty()) {
            list.add(
                VideoData(
                    body.firstName,
                    body.introVideo,
                    body.photo
                ).apply {
                layoutResId = R.layout.home_video_view
            })
        }

        if (!body.classes.isNullOrEmpty()) {
            list.addAll(body.classes!!)
        }
        liveData.value = list
    }
}