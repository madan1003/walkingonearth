package com.example.wone.home.model

import com.example.wone.Adapter.BaseTModel
import com.example.wone.home.model.Event
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class HomeDataModel(
    @SerializedName("teacherId")
    @Expose
    var teacherId: String? = null,

    @SerializedName("firstName")
    @Expose
    var firstName: String? = null,

    @SerializedName("lastName")
    @Expose
    var lastName: String? = null,

    @SerializedName("level")
    @Expose
    var level: String? = null,

    @SerializedName("photo")
    @Expose
    var photo: String? = null,

    @SerializedName("introVideo")
    @Expose
    var introVideo: String? = null,

    @SerializedName("bio")
    @Expose
    var bio: String? = null,

    @SerializedName("teachingAccreditations")
    @Expose
    var teachingAccreditations: String? = null,

    @SerializedName("classes")
    @Expose
    var classes: List<Event>? = null
) : BaseTModel()