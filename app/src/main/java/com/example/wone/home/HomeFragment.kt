package com.example.wone.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wone.Adapter.BaseRecyclerAdapter
import com.example.wone.Adapter.BaseTModel
import com.example.wone.BR
import com.example.wone.R
import com.example.wone.databinding.FragmentHomeBinding
import com.example.wone.home.model.VideoData
import com.example.wone.utility.BaseHandler
import com.example.wone.utility.VideoViewActivity

/*
* THis is main fragment showing instructor info and upcoming sessions
* */
class HomeFragment : Fragment(), BaseHandler<BaseTModel> {

    lateinit var binding: FragmentHomeBinding
    lateinit var adapter: BaseRecyclerAdapter<BaseTModel>
    lateinit var viewModel: HomeViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
    }

    /*
    * Creating View
    * Using Data Binding approach
    * */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentHomeBinding>(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )
        binding.setVariable(BR.handler, this)
        startObservingLiveData()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = BaseRecyclerAdapter(0, this)
        val rc = binding.itemList.apply {
            layoutManager = LinearLayoutManager(context)
        }
        rc.adapter = adapter
    }

    private fun startObservingLiveData() {
        viewModel.liveData.observe(HomeFragment@this, Observer {
            updateView(it)
            hideProgress()
        })

        viewModel.error.observe(HomeFragment@this, Observer {
            comingSoon(getString(R.string.some_thing_went_wrong))
            hideProgress()
        })
    }

    private fun hideProgress(){
        binding.progressBar.visibility = View.GONE
    }

    private fun updateView(it: List<BaseTModel>) {
        adapter.updateList(it)
    }

    fun openVideoView(uri: String) {
        val intent = Intent(activity, VideoViewActivity::class.java)
        intent.putExtra("INTENT_URI", uri)
        startActivity(intent)
    }

    override fun onClick(view: View, data: BaseTModel?) {
        when (view.id) {
            R.id.view_video -> (data as VideoData).url?.let { openVideoView(it) }
            R.id.button,
            R.id.session_card -> comingSoon(getString(R.string.coming_soon))
            R.id.back_button -> activity?.finish()
        }
    }

    private fun comingSoon(message: String) {
        Toast.makeText(
            context,
            message,
            Toast.LENGTH_SHORT
        ).show()
    }
}