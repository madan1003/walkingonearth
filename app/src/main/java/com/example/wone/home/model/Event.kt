package com.example.wone.home.model

import com.example.wone.Adapter.BaseTModel
import com.example.wone.R
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/*
* Data holding class
* */
class Event(
    @SerializedName("id")
    @Expose
    var id: String? = null,

    @SerializedName("name")
    @Expose
    var name: String? = null,

    @SerializedName("photo")
    @Expose
    var photo: String? = null
) : BaseTModel(layoutResId = R.layout.home_sessions_card)