package com.example.wone.utility

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

/*
* An Utility class to load an image URL over the iamge view
* Here other utility methods related to view can come in
* */
object DataBindingUtility {

    @JvmStatic
    @BindingAdapter("app:imageUrl")
    fun fundManager(
        imageView: ImageView,
        url: String?,
    ) {
        if (url?.isNullOrEmpty() == false) {
            Picasso.get().load(url).into(imageView);
        }
    }
}