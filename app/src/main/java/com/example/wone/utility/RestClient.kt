package com.example.wone.utility

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/*
* Right now I have created a Static rest client
* This can be done beautifully with the help of hilt ( A dependency injection)
* */
object RestClient {
    @JvmStatic
    var retrofitService: RetrofitService? = null

    fun getInstance(): RetrofitService {

        if (retrofitService == null) {
            val retrofit = retrofit(gson())
            retrofitService = retrofit.create(RetrofitService::class.java)
        }

        return retrofitService!!


    }

    fun gson(): Gson {
        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create()
    }

    fun retrofit(gson: Gson): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://demo3512151.mockable.io")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }
}