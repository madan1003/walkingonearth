package com.example.wone.utility

import android.view.View

/*
* THis class is working as Interface for xml on click events
* This can be used anywhere as an interface for call back mechanism
* */
interface BaseHandler<T> {
    fun onClick(view: View, data: T?)
}