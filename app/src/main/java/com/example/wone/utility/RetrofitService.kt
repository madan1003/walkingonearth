package com.example.wone.utility

import com.example.wone.home.model.HomeDataModel
import retrofit2.Call

import retrofit2.http.GET

/*
* Interface class for the ends needs to be called to fetch the data
* */
interface RetrofitService {
    @GET("homedata")
    fun getHomeData(
    ): Call<HomeDataModel>
}