package com.example.wone.Adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.wone.R
import com.example.wone.utility.BaseHandler

/*
* THis is base adapter for recycler view
* THis class can be used for any iterm type of recycler view
* This is capable of handling different item view type within same list view
* */
open class BaseRecyclerAdapter<T : BaseTModel> : RecyclerView.Adapter<BaseViewHolder> {

    private var layoutResource = 0
    var list: MutableList<T> = ArrayList()
    var handler: BaseHandler<*>? = null

    /**
     * basic function can make use of all below method if required
     *
     * @param
     */

    val items: List<T>?
        get() = list

    val firstItem: T?
        get() = if (list.size > 0) list[0] else null

    constructor(layoutResource: Int, handler: BaseHandler<*>?) {
        this.layoutResource = layoutResource
        this.handler = handler
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder {
        return try {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = DataBindingUtil.inflate<ViewDataBinding>(
                layoutInflater, viewType, parent, false
            )
            BaseViewHolder(binding)
        } catch (e: Exception) {
            Log.e("TAG", "Crash Exception ${parent.context}")
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = DataBindingUtil.inflate<ViewDataBinding>(
                layoutInflater, R.layout.item_dummy_crash, parent, false
            )
            BaseViewHolder(binding)
        }
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder,
        position: Int
    ) {
        val obj = list[position]
        holder.bind(obj, handler)
    }

    override fun getItemViewType(position: Int): Int {
        return if (layoutResource != 0) {
            /**
             * for single view type
             */
            layoutResource
        } else {
            /**
             * for different view types
             */
            list[position].layoutResId
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    /**
     * adding data for the first time in the list and scroll if required to last position
     *
     * @param li
     */
    open fun updateList(li: List<T>?) {
        if (li != null) {
            this.list.clear()
            this.list = ArrayList(li)
            notifyDataSetChanged()
        }
    }

    /**
     * insert data at the bottom of the list
     *
     * @param dataList
     */
    fun insertIntoBottomList(dataList: List<T>?) {
        var position = list.size
        if (dataList == null) {
            return
        }
        dataList.forEach { datumList ->
            list.add(position, datumList)
            notifyItemInserted(position)
            position++
        }
    }

    fun clearItems() {
        list.clear()
        notifyDataSetChanged()
    }

    fun getItem(position: Int): T? {
        return if (list.size > position) list[position] else null
    }

    fun isEmpty() = items?.isNotEmpty() != true
}
