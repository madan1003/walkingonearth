package com.example.wone.Adapter

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.wone.BR
import com.example.wone.utility.BaseHandler

/*
* This is view Holder for the recylcler view adapter class
* */
class BaseViewHolder(val binding: ViewDataBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(obj: Any?, handler: BaseHandler<*>?) {
        binding.setVariable(BR.obj, obj)
        binding.setVariable(BR.handler, handler)
        binding.executePendingBindings()
    }

}